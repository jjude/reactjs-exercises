# Reactjs Exercises

Excercises to learn React.js

Blog posts: [Learn React.js with Typescript](https://jjude.com/react-ts/)

01. Getting started with React.js and Typescript ✔ ️
02. Creating a simple React.js component with Typescript ✔ ️
03. Props vs State in React.js ✔ ️
04. Read user input from a form ✔ ️
05. Display list of items from a static array ✔ ️
06. Use bootstrap to style components
07. Make a counter with + & - which increment and decrement a value
