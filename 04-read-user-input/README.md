# Read input via form

This covers reading input via form and passing values to other components.

This introduces TimerInput component building on top of the previous example. This is a form with an input and button. User inputs a value and presses the button. The entered value is passed to the Timer component. Timer component starts ticking with that value.

Functions are first-class members in Javascript and hence can be passed around. A callback function is used as a props in TimerInput component to pass the value around. Invoke the TimerInput component from Timer component with a callback function as a prop.

Whenver the user clicks the button to set the value, invoke the callback sent as a prop. In the Timer component, process the passed in value (here, initialize the ticking value)
