import * as React from "react";
import * as ReactDOM from "react-dom";

import { Timer } from "./components/Timer";

ReactDOM.render(
  <Timer startWith={10} />,
  document.getElementById("main")
);
