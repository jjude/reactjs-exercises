import * as React from "react";

interface Props { onTimerInputSubmit: (startValue: number) => void }
interface State { }

export class TimerInput extends React.Component<Props, State>{
  constructor(props: Props) {
    super(props);

  }

  render() {
    const handleSubmit = (event: any) => {
      event.preventDefault();

      const timerValue = Number((this.refs["timerStartWith"] as HTMLInputElement).value.trim());
      // invoke the callback
      this.props.onTimerInputSubmit(timerValue)
    }

    return (
      <form onSubmit={handleSubmit}>
        <input type="text" placeholder="Timer start value" ref="timerStartWith" />
        <input type="submit" value="Submit" />
      </form>
    )
  }
}
