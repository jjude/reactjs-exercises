import * as React from "react";

import { TimerInput } from "./TimerInput";

interface Props { startWith: number }
interface State { currentValue: number }

export class Timer extends React.Component<Props, State> {
  // initialise state in constructor
  constructor(props: Props) {
    super(props);

    this.state = { currentValue: this.props.startWith }

    let timerId = setInterval(() => {
      this.setState((prevState: State, props: Props) => ({
        currentValue: prevState.currentValue - 1
      }))

      // if it reaches 0 then stop
      if (this.state.currentValue === 0) {
        clearInterval(timerId);
      }
    }, 1000);
  }

  render() {
    const handleInputValue = (startValue: number) => {
      this.setState({ currentValue: startValue });
    }
    return (
      <div className="Timer">
        <TimerInput onTimerInputSubmit={handleInputValue} />
        {this.state.currentValue}
      </div>
    )
  }
}
