# Creating a simple React.js component with Typescript

A react.js component could be a search-bar or a list of item. It is a general practice to break down the application into smallest components and build the application on top of the components.

```
+-------------------------+
|-----------+             | <---+ App component
||My todos  | <------------------+ Header component
|-----------+-----------+ |
||* code a program      | |
||* blog about it       |<-------+ Items component
||* share it in twitter | |
|-----------------------+ |
+-------------------------+
```

If we create a to-do app and display all the todos with a heading (say 'My todos') then there are three components -- the items, the header, and the app itself.

In this program, we create a simple react Hello component. This program builds on top of the previous program.

* as a general practice, components are created within a folder called components. so create a folder called `components` under `src`.
* create a `Hello.tsx`. This is our first component. We can pass parameters into components using this.props.
* Run using `yarn run build` and open index.html

Detailed blog post: https://jjude.com/react-component-with-tsc/
