# Getting started with Reactjs and Typescript

Create a simple React.js "Hello World" program with Typescript

Ref: https://medium.com/@fay_jai/getting-started-with-reactjs-typescript-and-webpack-95dcaa0ed33c

1. mkdir src build
2. initiate with `yarn init -y`
3. `yarn add react react-dom --modules-folder ~/node_modules/`
3. `yarn add -D typescript webpack ts-loader @types/react @types/react-dom --modules-folder ~/node_modules/`
4. install webpack globally so that we can invoke it without prefixing with path (`yarn global add webpack`)
5. add webpack.config.js
6. add tsconfig.json
7. add a build step in script section of package.json
8. create index.html in root directory and index.tsx in src directory
9. `yarn run build`
10. open index.html in browser

Detailed blog post: https://jjude.com/react-with-tsc/
