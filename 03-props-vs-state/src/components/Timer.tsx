import * as React from "react";

interface Props { startWith: number }
interface State { currentValue: number }

export class Timer extends React.Component<Props, State> {
  // initialise state in constructor
  constructor(props: Props) {
    super(props);

    this.state = { currentValue: this.props.startWith }

    setInterval(() => {
      this.setState((prevState: State, props: Props) => ({
        currentValue: prevState.currentValue - 1
      }))
    }, 1000);
  }

  render() {
    return (
      <div className="Timer">
        {this.state.currentValue}
      </div>
    )
  }
}
