# Difference between props and state in React.js

`props` and `state` of a component looks the same and leads to confusion.

Ref: http://lucybain.com/blog/2016/react-state-vs-pros/

Props = properties. Properties can be passed to a component. The component can't change properties.

State on the other hand can be created and changed by a component. State can be created by component in the constructor and can be changed by `setState`.

Let us build a timer component to understand `props` and `state`.

We are going to display a timer on the browser. Updating the timer value is done with `state`. The initial value will be passed via `props`.

Detailed blog post: https://jjude.com/props-vs-state-reactjs/
