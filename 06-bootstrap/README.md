# Style React.js components with Bootstrap

Bootstrap is a CSS library for styling websites and webapps. In this tutorial we use Bootstrap to style the "Countries and capitals" that we developed in the earlier tutorial.

1. Add a link to boostrap library (http://getbootstrap.com/getting-started/) into index.html
2. css class attributes are named as className; label for should be named as label htmlFor
3. This also uses shorter form of components without class
