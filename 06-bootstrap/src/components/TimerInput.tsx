import * as React from "react";

interface Props { onTimerInputSubmit: (startValue: number) => void }
interface State { }

export class TimerInput extends React.Component<Props, State>{
  constructor(props: Props) {
    super(props);

  }

  render() {
    const handleSubmit = (event: any) => {
      event.preventDefault();

      const timerValue = Number((this.refs["timerStartWith"] as HTMLInputElement).value.trim());
      // invoke the callback
      this.props.onTimerInputSubmit(timerValue)
    }

    return (
      <form onSubmit={handleSubmit} className="form-inline">
        <div className="form-group">
          <label htmlFor="timerValue">Timer Start Value:</label>
          <input type="text" className="form-control" placeholder="Timer start value" ref="timerStartWith" />
        </div>
        <button type="submit" className="btn btn-primary">Submit</button>
      </form>
    )
  }
}
