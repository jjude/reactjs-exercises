import * as React from "react";

import { Header } from "./Header";
import { Timer } from "./Timer";

export const App = () => (
  <div className="row">
    <div className="col-md-4 col-md-offset-3">
      <Header />
      <Timer startWith={150} />
    </div>
  </div>
);
