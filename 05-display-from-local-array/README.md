# Display values from local array

We will display countries and capitals using JSON.

This contains App, Header, List, and Item component.

* App is the shell that contains all the components
* Header displays the header
* List contains the list of countries and their capital in a JSON. It loops through the array and invokes Item component with name and capital as props
* Item component displays the passed in name and capital
