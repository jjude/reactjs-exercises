import * as React from "react";

import { Item } from "./Item";

interface Props { }
interface State { }

export class List extends React.Component<Props, State>{
  constructor(props: Props) {
    super(props);
  }

  render() {
    const countries = [
      { name: "India", capital: "Delhi" },
      { name: "Belgium", capital: "Brussels" },
      { name: "France", capital: "Paris" },
      { name: "Greece", capital: "Athens" }
    ]

    return (
      <ul>
        {
          countries.map(country => <Item name={country.name} capital={country.capital} />)
        }
      </ul>
    );
  }
}
