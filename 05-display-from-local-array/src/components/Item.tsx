import * as React from "react";


interface Props {
  name: string;
  capital: string;
}
interface State { }

export class Item extends React.Component<Props, State>{
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <li>
        Capital of {this.props.name} is {this.props.capital}
      </li>
    );
  }
}
