import * as React from "react";

import { Header } from "./Header";
import { List } from "./List";

interface Props { }
interface State { }

export class App extends React.Component<Props, State>{
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Header />
        <List />
      </div>
    );
  }
}
